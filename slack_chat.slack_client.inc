<?php

/**
 * @file
 * Contains the SlackClient class.
 */

class SlackClient {
  private static $client;

  /**
   * Instantiates an object using the access into in the slack_chat_access_info
   * variable.
   */
  public static function getClient() {
    if (self::$client == NULL) {
      $access_info = variable_get('slack_chat_access_info', array());
      $access_token = isset($access_info['access_token']) ? $access_info['access_token'] : '';
      $bot_access_token = isset($access_info['bot_access_token']) ? $access_info['bot_access_token'] : '';
      self::$client = new SlackClient($access_token, $bot_access_token);
    }

    return self::$client;
  }

  /**
   * Constructor.
   */
  public function __construct($access_token, $bot_access_token) {
    $this->accessToken = $access_token;
    $this->botAccessToken = $bot_access_token;
  }

  /**
   * Makes an oath authentication callback.
   * See https://api.slack.com/docs/oauth.
   *
   * @param $client_id
   *   The id of the app being authenticated.
   * @param $client_secret
   *   The secret of the app being authenticated.
   * @param $code
   *   Code returned by the oauth authentication.
   * @param $redirect_uri
   *   Redirect uri used for oauth authentication.
   *
   * @return
   *   Api response, or FALSE on error.
   */
  public function authenticate($client_id, $client_secret, $code, $redirect_uri) {
    $params = array(
      'client_id' => $client_id,
      'client_secret' => $client_secret,
      'code' => $code,
      'redirect_uri' => $redirect_uri,
    );

    return $this->apiCall('oauth.access', $params);
  }

  /**
   * Makes an api call to Slack.
   *
   * @param $method
   *   Method name to call.
   * @param $params
   *   Parameters to pass.
   * @param $use_token
   *   Specifies which token to send along with the request. Possible values:
   *     'global': Use the global token associated to the app.
   *     'bot': Use the token associated to bot user.
   *     FALSE: Don't send any token.
   * @param $return_error
   *   If true, the api response will be returned on error. Otherwise FALSE
   *   will be returned.
   *
   * @return
   *   Response, or FALSE on error.
   */
  public function apiCall($method, $params, $use_token = 'global', $return_error = FALSE) {
    $url = SLACK_CHAT_API_URL . $method;

    if ($use_token && !empty($this->accessToken) && $method != 'oauth.access') {
      $params['token'] = ($use_token == 'bot' ? $this->botAccessToken : $this->accessToken);
    }

    $options = array(
      'method' => 'POST',
      'headers' => array(
        'Content-Type' => 'application/x-www-form-urlencoded',
      ),
      'data' => http_build_query($params),
    );
    $response = drupal_http_request($url, $options);

    if ($response->code != 200) {
      watchdog('slack_chat', 'Slack API call returned status code @code. Error: @error', array('@code' => $response->code, '@error' => $response->error), WATCHDOG_ERROR);
      return FALSE;
    }

    $data = json_decode($response->data, TRUE);
    if (empty($data['ok'])) {
      watchdog('slack_chat', 'Slack API command @method failed. Error: @error', array('@method' => $method, '@error' => (!empty($data['error']) ? $data['error'] : 'unknown')), WATCHDOG_ERROR);
      return $return_error ? $data : FALSE;
    }

    return $data;
  }

  /**
   * Retrieves a list of channels.
   *
   * @param $exclude_archived
   *   If true, archived channels will not be returned.
   * @param $names_only
   *   If true, an array of channel names will be returned.
   *
   * @return
   *   Array of channels, or FALSE on error.
   */
  public function getChannelList($exclude_archived = TRUE, $names_only = FALSE) {
    $response = $this->apiCall('channels.list', array('exclude_archived' => ($exclude_archived ? 1 : 0)));
    if (!$response) {
      return FALSE;
    }

    $channels = $response['channels'];

    if ($names_only) {
      $channel_names = array();

      foreach ($channels as $channel) {
        $channel_names[] = $channel['name'];
      }

      return $channel_names;
    }
    else {
      return $channels;
    }
  }

  /**
   * Retrieves the message history for a channel from Slack.
   *
   * @param $channel_id
   *   The Slack channel id to send the message to.
   *
   * @return
   *   Array of messages, or FALSE on error.
   */
  public function getChannelHistory($channel_id) {
    $params = array(
      'channel' => $channel_id,
    );
    $response = $this->apiCall('channels.history', $params);
    if (!$response) {
      return FALSE;
    }

    $messages = array();
    foreach ($response['messages'] as $message) {
      if ($message['type'] != 'message' || (!empty($message['subtype']) && $message['subtype'] != 'bot_message')) {
        continue;
      }

      if (!empty($message['username'])) {
        // This is a bot message. The username is available in the message.
        $username = $message['username'];
      }
      else {
        $user_info = $this->getUserInfo($message['user']);

        if (!empty($user_info['profile']['real_name'])) {
          $username = $user_info['profile']['real_name'];
        }
        else {
          $username = $user_info['name'];
        }
      }

      $messages[] = array(
        'username' => $username,
        'text' => $message['text'],
        'ts' => $message['ts'],
      );
    }

    usort($messages, 'slack_chat_messages_sort');

    return $messages;
  }

  /**
   * Retrieves a user's info.
   *
   * @param $user_id
   *   The Slack user id.
   *
   * @return
   *   Array of user info, or FALSE on error.
   */
  function getUserInfo($user_id) {
    static $users = array();

    if (isset($users[$user_id])) {
      return $users[$user_id];
    }

    $params = array(
      'user' => $user_id,
    );
    $response = $this->apiCall('users.info', $params);
    if (!$response) {
      return FALSE;
    }

    $users[$user_id] = $response['user'];
    return $response['user'];
  }

  /**
   * Sends a message to a channel or user.
   *
   * @param $username
   *   The username to send the messages as. Not effective if $as_bot is true.
   * @param $channel_id
   *   The Slack channel id or user id to send the message to.
   * @param $message
   *   Message text.
   * @param $as_bot
   *   If true, the message is sent as the bot user, using the bot's access
   *   token.
   * @param $link_names
   *   If true, channel and user names in the text will be linked to the
   *   corresponding page when displayed in Slack.
   *
   * @return
   *   TRUE on success, FALSE on error.
   */
  function sendMessage($username, $channel_id, $message, $as_bot = FALSE, $link_names = FALSE) {
    $params = array(
      'channel' => $channel_id,
      'text' => htmlspecialchars($message, ENT_NOQUOTES),
    );

    if ($as_bot) {
      $params['as_user'] = TRUE;
    }
    else {
      $params['username'] = $this->getSanitizedName($username);
    }

    if ($link_names) {
      $params['link_names'] = 1;
    }

    $response = $this->apiCall('chat.postMessage', $params, $as_bot ? 'bot' : 'global');

    return $response ? TRUE : FALSE;
  }

  /**
   * Returns a sanitized text that can be used as user or channel name.
   *
   * @param $name
   *   The name to sanitize.
   */
  public function getSanitizedName($name) {
    return preg_replace('@[^a-z0-9_-]@', '_', substr(strtolower($name), 0, 20));
  }

  /**
   * Creates a channel.
   *
   * @param $channel_name
   *   The username to send the messages as.
   * @param $rename_existing
   *   If TRUE, the channel will be renamed when another channel already exists
   *   with the same name. Otherwise, the call will fail on duplicate names.
   *
   * @return
   *   Array containing the channel info, or FALSE on error.
   */
  function createChannel($channel_name, $rename_existing = FALSE) {
    $channel_name = $this->getSanitizedName($channel_name);
    $params = array(
      'name' => $channel_name,
    );
    $response = $this->apiCall('channels.create', $params, 'global', TRUE);

    // Find a non-existing name on conflict.
    if ($rename_existing && !empty($response['error']) && $response['error'] == 'name_taken') {
      $channels = $this->getChannelList(FALSE, TRUE);
      if (!$channels) {
        return FALSE;
      }

      while (in_array($channel_name, $channels)) {
        // If there is a number at the end, increment that. Add a number otherwise.
        if (preg_match('@^(.*)_([0-9]+)$@', $channel_name, $m)) {
          $i = $m[2] + 1;
          $channel_name = substr($m[1], 0, 17) . '_' . $i;
        }
        else {
          $channel_name = substr($channel_name, 0, 17) . '_1';
        }
      }

      $params['name'] = $channel_name;

      $response = $this->apiCall('channels.create', $params, 'global', TRUE);
    }

    if (empty($response['ok'])) {
      return FALSE;
    }

    return $response['channel'];
  }

  /**
   * Archives a channel.
   *
   * @param $channel_id
   *   Slack channel id.
   *
   * @return
   *   TRUE on success, or FALSE on error.
   */
  function archiveChannel($channel_id) {
    $params = array(
      'channel' => $channel_id,
    );
    $response = $this->apiCall('channels.archive', $params);

    return $response ? TRUE : FALSE;
  }

  /**
   * Sets the channel purpose.
   *
   * @param $channel_id
   *   Slack channel id.
   * @param $purpose
   *   Purpose text.
   *
   * @return
   *   Api response, or FALSE on error.
   */
  function setChannelPurpose($channel_id, $purpose) {
    $params = array(
      'channel' => $channel_id,
      'purpose' => $purpose,
    );
    $response = $this->apiCall('channels.setPurpose', $params);

    return $response;
  }

  /**
   * Invites a user to a channel.
   *
   * @param $channel_id
   *   Slack channel id.
   * @param $user
   *   Slack user id.
   *
   * @return
   *   Api response, or FALSE on error.
   */
  function invite($channel_id, $user_id) {
    $params = array(
      'channel' => $channel_id,
      'user' => $user_id,
    );
    $response = $this->apiCall('channels.invite', $params);

    return $response;
  }

}
