(function ($) {

  Drupal.slackChat = {};
  Drupal.slackChat.chats = [];

  /**
   * Constructor.
   *
   * @param $el
   *   The jQuery element to initialize the chat on.
   */
  Drupal.slackChat.chatModel = function ($el) {
    var _this = this;
    this.$el = $el;
    this.chatId = null;
    this.username = Drupal.t('Me');

    var chatId = $el.attr('data-chat-id');
    if (chatId) {
      // If the id is supplied, that means the chat is already started.
      this.chatId = chatId;

      // Put user in the nodejs channel created for them previously.
      if (Drupal.settings.slackChat.channel_tokens[chatId]) {
        Drupal.Nodejs.joinTokenChannel(chatId, Drupal.settings.slackChat.channel_tokens[chatId]);
      }

      // Retrieve the user name they entered when the chat was started.
      if (Drupal.settings.slackChat.username) {
        this.username = Drupal.settings.slackChat.username;
      }
    }

    $el.find('.slack-chat-start-button').click(function (e) {
      var $input = $(this).closest('.slack-chat-chat').find('.slack-chat-name-field');
      var name = $input.val();

      _this.startChat(name);
    });

    $el.find('.slack-chat-end-button').click(function (e) {
      _this.handleEnd();
    });

    $el.find('.slack-chat-send-button').click(function (e) {
      _this.handleSend();
    });

    $el.find('.slack-chat-message-field').keyup(function(e) {
      if (e.keyCode == 13 && !e.shiftKey && !e.ctrlKey) {
        _this.handleSend();
      }
      else {
        return true;
      }
    });
  };

  /**
   * Initiates a chat session.
   *
   * @param name
   *   The user's name.
   */
  Drupal.slackChat.chatModel.prototype.startChat = function (name) {
    var _this = this;
    var $button = this.$el.find('.slack-chat-start-button');
    $button.attr('disabled', 'disabled');

    this.makeAjaxCall({command: 'start_chat', name: name}, function (result) {
      $button.removeAttr('disabled');

      if (result === false || result.status != 'ok') {
        _this.showError(Drupal.t('We are unable to start a chat for you. Please try again later.'));
        console.log('Unable to start chat:' + result.status);
        return;
      }

      _this.$el.attr('data-chat-id', result.chat_id);
      _this.$el.addClass('slack-chat-started');
      _this.chatId = result.chat_id;
      _this.username = name;

      Drupal.Nodejs.joinTokenChannel(result.chat_id, result.channel_token);
    });
  };

  /**
   * Ends the chat session.
   */
  Drupal.slackChat.chatModel.prototype.endChat = function (notify) {
    this.$el.addClass('slack-chat-ended');

    this.makeAjaxCall({command: 'end_chat', notify: (notify ? 1 : 0)}, function (result) {
      if (result === false || result.status != 'ok') {
        console.log('Unable to end chat:' + result.status);
      }
    });
  };

  /**
   * Click handler for the end chat button.
   */
  Drupal.slackChat.chatModel.prototype.handleEnd = function () {
    this.endChat(true);
  };

  /**
   * Click handler for the send button. Checks if there is any message entered,
   * and sends it.
   */
  Drupal.slackChat.chatModel.prototype.handleSend = function () {
    var $input = this.$el.find('.slack-chat-message-field');
    var text = $input.val();

    if (!text) {
      return;
    }

    this.sendMessage(text);
    $input.val('');
  };

  /**
   * Sends a message to Slack.
   *
   * @param text
   *   The message to send.
   */
  Drupal.slackChat.chatModel.prototype.sendMessage = function (text) {
    var _this = this;

    this.printMessage(this.username, text);

    this.makeAjaxCall({command: 'send_message', text: text}, function (result) {
      if (result === false || result.status != 'ok') {
        _this.showError(Drupal.t('We are unable to send your message. Please try again later.'));
        console.log('Unable to send message:' + result.status);
      }
    });
  };

  /**
   * Displays an error message to the user.
   *
   * @param text
   *   Error text.
   */
  Drupal.slackChat.chatModel.prototype.showError = function (text) {
    var $error = this.$el.find('.slack-chat-error');

    $error.text(text);

    setTimeout(function () {
      $error.text('');
    }, 5000);
  };

  /**
   * Renders a message into the message container.
   *
   * @param user
   *   Username of the sender.
   * @param text
   *   Message text.
   */
  Drupal.slackChat.chatModel.prototype.printMessage = function (user, text) {
    var $messages = this.$el.find('.slack-chat-messages');
    user = user.replace(/</g, '&lt;').replace(/>/g, '&gt;');
    text = text.replace(/</g, '&lt;').replace(/>/g, '&gt;');

    var $name = $('<span class="slack-chat-message-user"></span>').html(user + ':');
    var $text = $('<span class="slack-chat-message-text"></span>').html(text);
    var $container = $('<div class="slack-chat-message-container"></div>');

    $container.append($name).append($text);
    $messages.append($container);

    this.scrollToBottom();
  };

  /**
   * Scrolls the message view to the last message.
   */
  Drupal.slackChat.chatModel.prototype.scrollToBottom = function (user, text) {
    var $messages = this.$el.find('.slack-chat-messages');
    var scrollTop = $messages[0].scrollHeight - $messages.height();
    $messages.scrollTop(scrollTop);
  };

  /**
   * Sends an ajax request to the server.
   *
   * @param params
   *   Parameters to send along with the request.
   * @param callback
   *   A function to call when the call finishes. First argument will be the
   *   returned data, or false on error.
   */
  Drupal.slackChat.chatModel.prototype.makeAjaxCall = function (params, callback) {
    params = params || {};
    params.token = Drupal.settings.slackChat.csrf_token;

    $.ajax({
      type: 'POST',
      url: Drupal.settings.slackChat.url,
      dataType: 'json',
      data: params,
      success: function (data, textStatus, XHR) {
        if (callback) {
          callback(data);
        }
      },
      error: function (XHR, textStatus, error) {
        console.log('Unable to send AJAX request:' + textStatus);
        if (callback) {
          callback(false);
        }
      }
    });
  };

  /**
   * Responds to an event received from node.js.
   *
   * @param message
   *   Message received from node.js
   */
  Drupal.slackChat.chatModel.prototype.handleEvent = function (message) {
    if (message.event == 'message') {
      this.printMessage(message.user, message.text);
    }
    else if (message.event == 'ended') {
      this.endChat(false);
    }
  };


  /**
   * Drupal behavior.
   */
  Drupal.behaviors.slackChat = {
    attach: function (context) {

      // Initialize chat object.
      $('.slack-chat-chat', context).each(function (index) {
        var chat = new Drupal.slackChat.chatModel($(this));
        Drupal.slackChat.chats.push(chat);
      });

      if (context == document) {
        // Click handler for the floating window header.
        $('.slack-chat-window-header').click(function (e) {
          var $window = $(this).closest('.slack-chat-floating-window');
          if ($window.hasClass('slack-chat-open')) {
            $window.removeClass('slack-chat-open');
          }
          else {
            $window.addClass('slack-chat-open');
          }

        });

        // Open chat window if chat is active.
        if (Drupal.settings.slackChat.chat_started) {
          $('.slack-chat-floating-window').addClass('slack-chat-open');
        }
      }

    }
  };

  /**
   * Called when a message is received from Slack.
   */
  Drupal.Nodejs.callbacks.slackChatHandler = {

    callback: function (message) {
      
      Drupal.slackChat.chats.forEach(function (chat) {
        if (chat.chatId && chat.chatId == message.channel) {
          chat.handleEvent(message);
        }  
      });

    }

  };

})(jQuery);


