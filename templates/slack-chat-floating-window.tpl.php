<div class="slack-chat-floating-window">
  <div class="slack-chat-window-header">
    <?php print $title; ?>
    <span class="slack-chat-window-close"></span>
  </div>

  <div class="slack-chat-window-content">
    <?php print $chat; ?>
  </div>
</div>
