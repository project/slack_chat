Slack chat module allows your visitors to send instant messages from your
website to your Slack account. Your visitors can use an embedded chat widget on
your site to chat with you, and you will receive their messages and respond to
them in Slack.

When a visitor starts a chat, a Slack bot will notify you of the new visitor. A
new Slack channel will be created for each visitor where you can chat with them
individually. Messages sent to these channels will be forwarded to the
corresponding visitor, and the visitors' messages will be forwarded to their
Slack channels. You can end a chat by archiving the channel in Slack.

Dependencies
------------

  * Slack chat depends on the Node.js integration module. You will need to
    install that module in Drupal, and the Node.js server application it uses
    as a backend. See https://www.drupal.org/project/nodejs

Installation
------------

  * Install Node.js integration module.

  * Install and enable this module.

  * This module comes with a Node.js extension that you will need to install on
    your Node.js server. The extension is located in the slack_chat_extension
    subdirectory. See the README.txt file in that subdirectory for
    instructions on how to install the extension.

  * You will need to create an app in Slack that Drupal can use to access your
    Slack account. You can read some background information about Slack apps
    here: https://api.slack.com/slack-apps
    Note: Slack has an "App Directory" where apps can be submitted to make it
    available to others. However, you will be creating an app just for your own
    team, and you will not need to submit it to the "App Directory".

    * Log in to your Slack account in the browser.

    * Go to https://api.slack.com/apps/new.

    * Fill in the App Name field. You can name your app however you like. This
      name will appear when the bot sends you messages about incoming chats.

    * Select your team in the Team field.

    * Enter an arbitrary description for the app in the "Short Description" and
      "Describe what your app does on Slack" field. For example, enter "This app
      enables my Drupal site to communicate with my team."

    * In the "Redirect URI(s)" field, enter the following URL:
      https://example.com/slack-chat/authorize, where "example.com" should be
      replaced with the domain of your site. Be sure the use https or http
      as appropriate depending on your setup. If your site lives in a
      subdirectory, enter that too
      (e.g. https://example.com/mydirectory/slack-chat/authorize).

    * Click the "Create App" button. Once your app is created, you will be taken
      to the app's configuration page.

    * Switch to the "Bot Users" tab, and click "Add a bot to this app" button.
      Name your bot, and click "Add bot user".

    * Switch to the "App Credentials" tab. You will find your app's Client ID
      and Client Secret here. Take note of these because you will need to enter
      these on your Drupal site.

  * On your Drupal site, go to Configuration>Slack chat
    (/admin/config/system/slack-chat). Enter the Client ID and Client Secret you
    copied from your Slack app, and click Save at the bottom.

  * The next step is to authorize this app to access your Slack channels and
    send you messages. To do so, click the "Authorize app" button on the Slack
    chat configuration page. You will be taken to the Slack website where you
    will need to confirm the authorization. Once you are taken back to your
    Drupal site, you should see a message saying that the authorization was
    successful.

  * Now that Drupal has access to your Slack account, you should see a list of
    your Slack channels in the "Channel for notifications" field. Select the one
    in which you wish to receive notifications about incoming chats, and save
    the configuration.

  * Visit Drupal's status page at Reports>Status report (/admin/reports/status)
    and ensure that there are no errors.

Usage
-----

  * There are three places where your visitors can chat: in a floating chat
    widget that appears in the corner of the page, in the Slack chat block, or
    on the /slack-chat page.
    If you wish to use the floating chat widget, go to the Slack chat
    configuration page, and enable the floating widget. Also specify which pages
    you would like the widget to be visible on.
    If you wish to use the block, go to the block configuration page, and
    place the block in the desired region.
    If you wish to use the page, place a link to /slack-chat somewhere on your
    site so that visitors can navigate to that page.

  * Visitors can start a chat by entering their name and clicking the "Start
    chat" button in the chat window. Once they do, Slack chat module will
    create a channel in your Slack account where you can chat with this visitor.
    The channel's name will consist of the visitor's first name, and an optional
    prefix you can configure. A message will also be sent to the Slack channel
    you selected to notify you of new chats.
    To start chatting with your visitor, join the Slack channel created for the
    visitor, and chat with them there. All messages in that channel will be
    visible to the visitor.
    Once you have finished chatting, archive the channel in Slack. This will
    notify the visitor that the chat has ended, and they won't be able to send
    messages anymore.
    If the visitor decides to end the chat first, the corresponding Slack
    channel will be automatically archived.

  * Additional options available on the configuration page:

    - Channel for notifications: Whenever a new chat is started by a visitor,
      a messages will be sent to this channel to notify you. You (the Slack user
      who authorized the app) need to be in this channel in order to see the
      notifications.

    - Channel prefix: This prefix will be prepended to channels created for
      visitors.

    - Enable floating chat widget: If checked, the chat widget will be shown on
      the selected pages.

    - Show floating chat on the following pages: Use this field to configure
      which pages should display the chat widget. This setting only applies to
      the floating chat widget. Enter one path per line, and use relative paths,
      e.g. "node/1". Use an asterisk to match an arbitrary string in the path,
      e.g. "node/*" will match all paths starting with "node/".

    - Chat title: The title to show in the floating chat widget and the block.
      When the floating widget is collapsed, this title will be visible.

    - Chat welcome message: A welcome message that displays at the top of the
      chat once the visitor has started the chat.

    - Chat ended message: A message that will appear when the chat has ended.

For more documentation, see slack_chat module's project page. Feel free to file
an issue in the issue queue if you have any feedback or questions.
https://www.drupal.org/project/slack_chat
