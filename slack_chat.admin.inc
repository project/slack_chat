<?php

/**
 * @file
 * Page callbacks for admin pages.
 */

/**
 * Form callback for the config form.
 */
function slack_chat_config_form($form, &$form_state) {
  $slack_client = SlackClient::getClient();

  $access_info = variable_get('slack_chat_access_info', array());
  $is_authorized = !empty($access_info['access_token']);
  $is_configured = (variable_get('slack_chat_client_id', '') != '' && variable_get('slack_chat_client_secret', '') != '');
  $channel_options = array();

  // Retrieve channel list, if the app is already authorized.
  if ($is_authorized) {
    $channels = $slack_client->getChannelList();

    if (!$channels) {
      drupal_set_message(t('Unable to retrieve channel list from Slack. Try reauthorizing the Slack app.'), 'error');
      $channel_options['_none'] = t('None');
    }
    else {
      foreach ($channels as $channel) {
        $channel_options[$channel['id']] = check_plain($channel['name']);
      }
    }
  }

  $form['slack_chat_app'] = array(
    '#type' => 'fieldset',
    '#title' => 'Slack app settings',
  );

  $form['slack_chat_app']['slack_chat_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID'),
    '#description' => t('The client ID of the app you created in Slack.'),
    '#required' => TRUE,
    '#default_value' => variable_get('slack_chat_client_id', ''),
  );

  $form['slack_chat_app']['slack_chat_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Client secret'),
    '#description' => t('The client secret of the app you created in Slack.'),
    '#required' => TRUE,
    '#default_value' => variable_get('slack_chat_client_secret', ''),
  );

  $form['slack_chat_app']['slack_chat_channel'] = array(
    '#type' => 'select',
    '#title' => t('Channel for notifications'),
    '#description' => t('Slack bot will send a message to a Slack channel when a visitor initiates a chat. Select which channel should be used for this purpose. Note that the user who authorized the app needs to be in this channel.') .
      (!$is_authorized ? ' ' . t('The channel list will be available after authorization.') : ''),
    '#options' => $channel_options,
    '#disabled' => !$is_authorized,
    '#required' => $is_authorized,
    '#default_value' => variable_get('slack_chat_channel', ''),
  );

  $form['slack_chat_app']['slack_chat_channel_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Channel prefix'),
    '#description' => t('You will chat with each visitor in a separate Slack channel. Specify a prefix to use when creating new channels.'),
    '#default_value' => variable_get('slack_chat_channel_prefix', 'drupal'),
    '#maxlength' => 10,
    '#size' => 10,
  );

  $form['slack_chat_app']['slack_chat_auth'] = array(
    '#type' => 'submit',
    '#value' => t('Authorize app'),
    '#limit_validation_errors' => array(),
    '#submit' => array('slack_chat_config_authorize_app_submit'),
    '#disabled' => !$is_configured,
  );

  if (!$is_configured) {
    $auth_note = t('Enter your app credentials above. Once the credentials are saved, click the "Authorize app" button to allow the app to access your Slack account.');
  }
  else if (!$is_authorized) {
    $auth_note = t('You need to authorize the Slack app in order to use Slack chat.');
  }
  else {
    $auth_note = t('Your Slack app has been connected to team %team.', array('%team' => $access_info['team_name']));
  }

  $form['slack_chat_app']['slack_chat_auth_note'] = array(
    '#markup' => $auth_note,
  );

  $form['slack_chat_appearance'] = array(
    '#type' => 'fieldset',
    '#title' => 'Appearance',
  );

  $form['slack_chat_appearance']['slack_chat_floating_chat_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable floating chat widget'),
    '#description' => t('If enabled, a floating chat widget will be shown at the bottom of the page where your visitors can start a chat. If you disable this option, you\'ll need to either activate the Slack chat block or direct your visitors to /slack-chat to start a chat.'),
    '#default_value' => variable_get('slack_chat_floating_chat_enabled', FALSE),
  );

  $form['slack_chat_appearance']['slack_chat_floating_chat_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Show floating chat on the following pages '),
    '#description' => t('List the paths of pages that should show the floating chat. Enter paths relative to your site\'s front page, one per line. The "*" character can be used as a wildcard. For example, "node/*" matches all paths starting with "node/". '),
    '#default_value' => variable_get('slack_chat_floating_chat_pages', '*'),
  );

  $form['slack_chat_appearance']['slack_chat_window_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Chat title'),
    '#description' => t('Enter a title for the chat widget. This title will be shown in the chat block and in the floating chat widget.'),
    '#default_value' => variable_get('slack_chat_window_title', t('Chat with us!')),
  );

  $form['slack_chat_appearance']['slack_chat_welcome_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Chat welcome message'),
    '#description' => t('Enter a text to show as a greeting when a visitor starts chatting.'),
    '#default_value' => variable_get('slack_chat_welcome_message', t('Welcome. How can we help you?')),
  );

  $form['slack_chat_appearance']['slack_chat_ended_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Chat ended message'),
    '#description' => t('Enter a text to display when the chat has ended.'),
    '#default_value' => variable_get('slack_chat_ended_message', t('This chat has ended.')),
  );

  $form['slack_chat_protection'] = array(
    '#type' => 'fieldset',
    '#title' => 'Protection',
  );

  $form['slack_chat_protection']['slack_chat_flood_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of chat sessions in the given time frame'),
    '#description' => t('Enter how many new chat sessions you are willing to accept in the time frame specified below.'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('slack_chat_flood_limit', 5),
  );

  $form['slack_chat_protection']['slack_chat_flood_window'] = array(
    '#type' => 'textfield',
    '#title' => t('Time frame'),
    '#description' => t('Specify the time frame in minutes for the chat session limit.'),
    '#size' => 5,
    '#maxlength' => 5,
    '#default_value' => variable_get('slack_chat_flood_window', 30),
  );

  return system_settings_form($form);
}

/**
 * Validation callback for the config form.
 */
function slack_chat_config_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['slack_chat_flood_limit'])) {
    form_set_error('slack_chat_flood_limit', t('Please enter a number in the session limit field.'));
  }
  if (!is_numeric($form_state['values']['slack_chat_flood_window'])) {
    form_set_error('slack_chat_flood_window', t('Please enter a number in the time frame field.'));
  }
}

/**
 * Submit handler for the 'authorize' button.
 */
function slack_chat_config_authorize_app_submit($form, &$form_state) {
  $params = array(
    'client_id' => variable_get('slack_chat_client_id', ''),
    'scope' => SLACK_CHAT_AUTH_SCOPES,
    'redirect_uri' => url('slack-chat/authorize', array('absolute' => TRUE)),
    'state' => drupal_get_token('slack_chat'),
  );

  $auth_url = url(SLACK_CHAT_AUTH_URL, array('external' => TRUE, 'query' => $params));

  if (module_exists('overlay') && overlay_get_mode() == 'child') {
    // If the overlay is open, we are in an iframe. Close the overlay so that
    // the whole page redirects.
    unset($_GET['destination']);
    overlay_close_dialog($auth_url, array('external' => TRUE));

    $form_state['redirect'] = FALSE;
  }
  else {
    $form_state['redirect'] = $auth_url;
  }
}

/**
 * Page callback for the authorization page.
 */
function slack_chat_authorize_page() {
  $slack_client = SlackClient::getClient();

  // When Slack redirects back to this page, it should either pass 'error' or
  // 'code' in the query string. 'state' is an arbitrary parameter that can be
  // passed to Slack and will be passed back as is. Used as a verification token
  // here.
  if (!empty($_GET['error'])) {
    drupal_set_message(t('Authorization failed. Please try again.'), 'error');
    drupal_goto('admin/config/system/slack-chat');
  }
  else if (empty($_GET['code']) || empty($_GET['state']) || !drupal_valid_token($_GET['state'], 'slack_chat')) {
    return MENU_ACCESS_DENIED;
  }

  $response = $slack_client->authenticate(variable_get('slack_chat_client_id', ''), variable_get('slack_chat_client_secret', ''), $_GET['code'], url('slack-chat/authorize', array('absolute' => TRUE)));
  if (!$response) {
    drupal_set_message(t('Authorization failed. Check the log messages for more details.'), 'error');
    drupal_goto('admin/config/system/slack-chat');
  }

  $access_info = array(
    'access_token' => !empty($response['access_token']) ? $response['access_token'] : '',
    'user_id' => !empty($response['user_id']) ? $response['user_id'] : '',
    'team_id' => !empty($response['team_id']) ? $response['team_id'] : '',
    'team_name' => !empty($response['team_name']) ? $response['team_name'] : '',
    'bot_user_id' => !empty($response['bot']['bot_user_id']) ? $response['bot']['bot_user_id'] : '',
    'bot_access_token' => !empty($response['bot']['bot_access_token']) ? $response['bot']['bot_access_token'] : '',
  );
  variable_set('slack_chat_access_info', $access_info);

  drupal_set_message('The authorization has been successful.');

  $response = Nodejs::httpRequest('nodejs/slack_chat/reset', array('method' => 'POST'));
  if (!$response || $response->status != 'success') {
    drupal_set_message('An error occurred while trying to communicate with the Node.js server. Please verify the Node.js server configuration.', 'error');
  }

  drupal_goto('admin/config/system/slack-chat');
}

