<?php

/**
 * @file
 * Page callback functions.
 */

/**
 * Page callback for the chat page.
 */
function slack_chat_page() {
  return array(
    '#theme' => 'slack_chat_chat',
  );
}
